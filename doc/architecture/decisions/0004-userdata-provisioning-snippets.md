# 4. Userdata provisioning snippets

Date: 2022-11-29

## Status

Accepted

## Context

A solution is needed to automatically deploy the necessary services on monitoring servers. Currently, after initializing the servers, you need to manually deploy monitoring services using an Ansible script.

## Decision

The provisioning process with Terraform uses the user_data script to customize the installation during server initialization. Parts of this setup will be common to most projects, such as installing Ansible and Docker, applying package updates, etc.

Added Prometheus and Grafana deployment process to this user_data script.

## Consequences

After initializing the servers using Terraform, we get servers ready for operation, in the future the scripts will be updated as necessary.