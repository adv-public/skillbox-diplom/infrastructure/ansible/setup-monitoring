# 3. DNS infrastructure

Date: 2022-11-29

## Status

Accepted

## Context

Services and application endpoints must be available on the Internet and resolved by public DNS. For example, grafana.denav, monitoring.denav, test.denav, etc.

Add automatic records management in root domains, the denav.net domain zone is located at the Hetzner provider.

## Decision

Use the “hetznerdns” module in Terraform, which will allow you to manage DNS records at this stage. In the future, consider transferring the domain zone to AWS Route53.

## Consequences

Records management in root domains will be automated, we won't have to configure manually.
