# 2. Choice of monitoring system

Date: 2022-11-22

## Status

Accepted

## Context

We need to choose a platform to monitor our web cluster hosted in Amazon Web Services. Our application (skillbox-app), located on these servers, returns metrics in the Prometheus format.

## Decision

We use Prometheus and Grafana in conjunction to monitor our systems, the systems will be configured using docker-compose. To automate the configuration of monitoring systems, Ansible will be used.

We use Amazon Web Services as our hosting provider of choice. This will facilitate further maintenance and support of the systems.

## Consequences

We will have to migrate our infrastructure from our current hosting provider to Amazon, update the project for Ansible.
